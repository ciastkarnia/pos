# Define the available cakes and their prices
cakes = {
    "Chocolate": 10,
    "Vanilla": 8,
    "Strawberry": 12,
    "Red Velvet": 15
}

# Initialize the total sales variable
total_sales = 0

# Loop to keep the program running
while True:
    # Print the available cakes and their prices
    print("Available Cakes:")
    for cake, price in cakes.items():
        print(f"{cake}: ${price}")
    
    # Ask the customer which cake they want to buy
    choice = input("Enter the cake you want to buy (or 'exit' to exit): ")
    
    # Check if the customer wants to exit
    if choice.lower() == "exit":
        break
    
    # Check if the chosen cake is available
    if choice in cakes:
        # Ask the customer for the quantity of the chosen cake
        quantity = int(input("Enter the quantity: "))
        
        # Calculate the total cost for the chosen cake and quantity
        cost = cakes[choice] * quantity
        
        # Add the cost to the total sales
        total_sales += cost
        
        # Print the order details
        print(f"Order Details:")
        print(f"Cake: {choice}")
        print(f"Quantity: {quantity}")
        print(f"Cost: ${cost}")
    else:
        # Print an error message if the chosen cake is not available
        print("Invalid choice, please try again.")
    
# Print the total sales
print(f"Total Sales: ${total_sales}")
